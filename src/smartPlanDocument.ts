'use strict';

import { TextDocument, Uri } from 'vscode';
import SmartPlan from './smartPlan';
import * as path from 'path';

export default class SmartPlanDocument {
    constructor(private _path: string, private _text: string) {
    }

    get previewScript() {
        let name = path.basename(this._path, ".json");
        var plan = new SmartPlan(name, this._text);
        return plan.previewScript;
    }

    static convertToJsExtension(fsPath: string): string {
        return fsPath.substring(0, fsPath.lastIndexOf('.')) + ".js";
    }
}
