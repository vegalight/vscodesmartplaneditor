'use strict';

import { TextDocumentContentProvider, EventEmitter, Uri, Disposable } from 'vscode';
import { workspace, window, TextDocument } from 'vscode';
import * as fs from 'fs';
import * as path from 'path';
import SmartPlanDocument from './smartPlanDocument';

export default class SmartPlanContentProvider implements TextDocumentContentProvider {
	static scheme = 'javascript';

	private _map = new Map<string, string>();

	private _onDidChange = new EventEmitter<Uri>();

	constructor() {
	}

	dispose() {
		this._onDidChange.dispose();
	}

	getAndMapUri(uri: Uri) : Uri {
		let virtualUri = this.getVirtualUri(uri.fsPath);
		this._map.set(virtualUri.fsPath.toUpperCase(), uri.fsPath.toUpperCase());
		return virtualUri;
	}

	requiresUpdate(fsPath: string) {
		return [...this._map.values()].some(x => x === fsPath.toUpperCase());
	}

	get onDidChange() {
		return this._onDidChange.event;
	}

	update(uri: Uri) {
		this._onDidChange.fire(Uri.parse(uri.toString().toLowerCase()));
	}

	provideTextDocumentContent(uri: Uri): string | Thenable<string> {
		let realPath = this._map.get(uri.fsPath.toUpperCase());
		let text = fs.readFileSync(realPath, "utf-8");
		let document = new SmartPlanDocument(realPath, text);
		return document.previewScript;
	}

	getVirtualUri(fsPath: string): Uri {
		var convertedPath = SmartPlanDocument.convertToJsExtension(fsPath);
		var fileName = "preview-" + path.basename(convertedPath).toLowerCase();
		return Uri.parse(`${SmartPlanContentProvider.scheme}:smart-plan/${fileName}`);
	}
}