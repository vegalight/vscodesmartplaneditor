'use strict';

import * as vscode from 'vscode';
import * as fs from 'fs';
import * as os from 'os';
import * as path from 'path';
import SmartPlanEditorsManager from './smartPlanEditorsManager';
import SmartPlanContentProvider from './smartPlanContentProvider';
import SmartPlanDocument from './smartPlanDocument';
import SmartPlan from './smartPlan';

const tempdir = vscode.workspace.getConfiguration('smartplan-editor').get('tempDir') || os.tmpdir()

let editorsManager = new SmartPlanEditorsManager(tempdir.toString());

export function activate(context: vscode.ExtensionContext) {

    let provider = new SmartPlanContentProvider();

    let providerRegistration = vscode.Disposable.from(
        vscode.workspace.registerTextDocumentContentProvider(SmartPlanContentProvider.scheme, provider)
    );

    vscode.workspace.onDidChangeTextDocument((e: vscode.TextDocumentChangeEvent) => {
        if (provider.requiresUpdate(e.document.uri.fsPath)) {
            provider.update(provider.getVirtualUri(e.document.uri.fsPath));
        }
    }, null, context.subscriptions);

    vscode.workspace.onDidCloseTextDocument((document: vscode.TextDocument) => {
        // var path = document.uri.fsPath.toUpperCase();
        // if (document.uri.scheme == "git") {
        //     path = JSON.parse(document.uri.query).path.toUpperCase();
        // }
        // editorsManager.deleteEditFile(path);

    }, null, context.subscriptions);

    vscode.workspace.onDidSaveTextDocument(event => {
        editorsManager.saveTrackedOriginalFile(event.fileName, event.getText());
    }, null, context.subscriptions);

    let editRegistration = vscode.commands.registerTextEditorCommand('extension.smartPlanEdit', editor => {
        try {
            var editFilePath = editorsManager.getAndMapEditFilePath(editor.document);

            vscode.workspace.openTextDocument(editFilePath).then(doc => vscode.window.showTextDocument(doc, editor.viewColumn + 1))
        } catch (e) {
            vscode.window.showErrorMessage("Unable to edit smart plan : " + e.toString());
        }
    });

    let previewRegistration = vscode.commands.registerTextEditorCommand('extension.smartPlanPreview', editor => {
        var virtualUri = provider.getAndMapUri(editor.document.uri);

        vscode.workspace.openTextDocument(virtualUri).then(doc => vscode.window.showTextDocument(doc, editor.viewColumn + 1))
    });

    let clearCacheRegistration = vscode.commands.registerTextEditorCommand('extension.smartPlanEditClearCache', editor => {
        var virtualUri = provider.getAndMapUri(editor.document.uri);
        editorsManager.clear();
    });

    context.subscriptions.push(providerRegistration, previewRegistration, editRegistration, clearCacheRegistration);
}

export function deactivate() {
    editorsManager.deleteAllEditFiles();
}
