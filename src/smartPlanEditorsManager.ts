'use strict';

import { TextDocumentContentProvider, EventEmitter, Uri, Disposable } from 'vscode';
import { workspace, window, TextDocument } from 'vscode';
import * as fs from 'fs';
import * as path from 'path';
import SmartPlan from './smartPlan';
import SmartPlanDocument from './smartPlanDocument';

export default class SmartPlanEditorsManager {
	static scheme = 'javascript';

	private _map = new Map<string, string>();

	constructor(private _tempDir: string) {
	}

	getAndMapEditFilePath(document: TextDocument): string {
		var fileName = path.basename(SmartPlanDocument.convertToJsExtension(document.uri.fsPath));
		var tmpFilePath = `${this._tempDir}${path.sep}${fileName}`;

		var entry = this._map.get(document.uri.fsPath.toUpperCase());
		if (!entry) {
			var plan = new SmartPlan("", document.getText());
			fs.writeFile(tmpFilePath, plan.script, console.error);

			this._map.set(document.uri.fsPath.toUpperCase(), tmpFilePath.toUpperCase());
		}

		return tmpFilePath;
	}

	saveTrackedOriginalFile(fsPath: string, text: string) {
		this._map.forEach((value, key, map) => {
			if (value === fsPath.toUpperCase()) {
				var orig = fs.readFile(key, "utf-8", (e, data) => {
					try {
						data = this.stipByteOrderMark(data);
						var json = JSON.parse(data.toString());
						json.script = text;

						fs.writeFile(key, JSON.stringify(json), (err) => {
							if (err) throw err;
							
							window.showInformationMessage("Smart plan saved");
						});
					}
					catch (e) {
						window.showErrorMessage("Unable to save smart plan : " + e.toString());
					}
				});
			}
		});
	}

	clear() {
		this._map.clear();
	}

	deleteEditFile(fsPath: string) {
		this._map.forEach((value, key, map) => {
			if (fsPath.toUpperCase() === value) {
				fs.unlink(value, console.error);
				map.delete(key);
				return;
			}
		});
	}

	deleteAllEditFiles() {
		this._map.forEach((value, key, map) => {
			fs.unlink(value, console.error);
		});
	}

	private stipByteOrderMark(x: string) {
		if (x.charCodeAt(0) === 0xFEFF) {
			return x.slice(1);
		}
	
		return x;
	}
}