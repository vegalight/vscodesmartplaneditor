import { TextDocument } from "vscode";

'use strict';

export default class SmartPlan {
    constructor(private _name: string, private _text: string) {
    }

    get script() {
        try {
            let json = JSON.parse(this._text);
            let script = json.script;

            if (script === null)
                return "No Smart Plan found in JSON document";

            return script;
        } catch (e) {
            return "This is not a JSON document";
        }
    }

    get previewScript() {
        try {
            let json = JSON.parse(this._text);
            let script = json.script;

            if (script === null)
                return "No Smart Plan found in JSON document";

            let properties = Object.keys(json).filter(x => x !== "script");

            let propertiesText = properties
                .reduce((acc, key, index, keys) => {
                    var value = json[key];
                    if (Array.isArray(value) || typeof value === "object"){
                        value = JSON.stringify(value);
                    }

                    var updated = acc + `// ${key}: ${value}`;
                    if (index < keys.length - 1)
                        updated += "\n";
                    return updated;
                }, "");

            let enrichedScript =
                `// ======================================================
// name: ${this._name.toLowerCase()} 
// =======================================================
//
${propertiesText}
// =======================================================
            
${script}
`

            return enrichedScript;


        } catch (e) {
            return "This is not a JSON document";
        }
    }
}
