# Visual Studio Code Smart Plan Editor

Smart Plan Editor for Visual Studio Code allows you to easily preview and edit Smart Plan JSON files.

**How does it work?**

VS Code has the concept of virtual documents, which can be used to load in a read-only window based on a URI.

In order to support editing, a temp file is created (in the OS defined temp folder) and then deleted when either the editor window or VS Code is closed. 